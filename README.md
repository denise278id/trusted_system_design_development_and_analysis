# README #

•	560: Trusted System Design, Development and Analysis
o	Methodology for developing Trusted Systems that can withstand attacks by skilled, motivated adversaries with high assurance; Design and implementation of Reference Monitors and alternative security architectures; Design considerations for being able to evaluate the security of a system
	Organizational Objectives
	Threat Modeling
	Threat Analysis
	Risk Assessment
	Security Requirements
	Risk Management Framework
	Security Design Principles
	Authentication and Authorization
	Gap Analysis
